<?php
$gender = array(0 => "Nam", 1 => "Nữ");
$department = array("MAT" => "Khoa học máy tính", "KDL" => "Khoa học vật liệu");
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sign up for new student</title>
    <style>
    .background {
        display: flex;
        margin-top: 2rem;
        align-items: center;
    }

    .signup {
        width: 25rem;
        margin: auto;
        margin-top: 4.5rem;
        padding: 3.8rem 2.8rem 3.8rem 2.8rem;
        border: solid 2px #4e7aa3;
    }

    .signup-form {
        display: flex;
        font-size: 18px;
        margin-bottom: -1rem;
    }

    .signup-form-text {
        width: 90px;
        padding: 0.4rem 0.6rem;
        margin-right: 1rem;
        text-align: center;
        color: white;
        background-color: #5b9bd5;
        border: 2px solid #4e7aa3;
    }

    input[type="text"] {
        width: 60%;
        height: 2.4rem;
        margin-top: 1rem;
        padding-left: 0.5rem;
        border: 2px solid #4e7aa3;
    }

    .gender {
        display: flex;
        align-items: center;
        width: 130px;
    }

    select {
        margin-top: 1rem;
        outline: none;
        width: 40%;
        height: 2.8rem;
        padding-left: 0.5rem;
        border: 2px solid #4e7aa3;
    }

    .signup-button {
        justify-content: center;
        margin-top: 2rem;
    }

    input[type="submit"] {
        height: 2.8rem;
        width: 8rem;
        cursor: pointer;
        color: white;
        background-color: #70ad46;
        border-radius: 8px;
        border: solid 2px #477990;
    }
    </style>
</head>
<body>
    <div class="background">
        <div class="signup">
            <form action="" method=" POST">
                <div class="signup-form">
                    <p class="signup-form-text">Họ và tên</p>
                    <input required name="fullName" type="text">
                </div>
                <div class="signup-form">
                    <p class="signup-form-text">Giới tính</p>
                    <div class="gender">
                        <?php for($i = 0; $i <= 1; $i++) : ?>  
                        <input type='radio' name='gender'>
                        <label style="margin-right: 15px" for=<?= $i ?>>
                            <?= $gender[$i]; ?>
                        </label>
                        <?php endfor; ?>
                    </div>
                </div>
                <div class="signup-form">
                    <p class="signup-form-text">Phân khoa</p>
                    <select required name="department" id="department">
                        <option disabled selected value></option>
                        <?php foreach ($department as $key => $value) : ?>
                        <option value="<?= $key ?>"><?= $value ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="signup-form signup-button">
                    <input type="submit" value="Đăng ký">
                </div>
            </form>
        </div>
    </div>
</body>
</html>